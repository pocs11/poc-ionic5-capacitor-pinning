# poc-ionic5-capacitor-pinning

POC para validar o uso de pinning em projetos ionic 5 utilizando o capacitor

## Suporte

Android e iOS

## Instalação

```
npm install
```

## Execução

```
ionic capacitor [platform]
```

Necessário ter instalado o Android Studio e/ou XCode.

## Utilização

Nesta aplicação, a pagina HOME apresenta 2 botões, `Certificado` e `Não Certificado`.

Ao pressionar o `Certificado`, o console irá apresentar o json de retorno de uma requisição completada com sucesso.

Ao pressionar o `Não Certificado`, o console irá apresentar o json de retorno com o seguinte erro:

```javascript
{
    status: -2, 
    error: "TLS connection could not be established: javax.net…n: Trust"
     "anchor for certification path not found."
}
```

Ao iniciar a aplicação, será realizada uma validação, retornando `"Congratulaions, you have set up SSL Pinning."` caso haja sucesso, e `"Opss, SSL pinning failed."` caso o pinning falhe.


## Referências

https://github.com/ashenwgt/ionic-capacitor-ssl-pinning

https://www.otricks.com/how-to-implement-ssl-pinning-in-ionic-5-or-html5-app/