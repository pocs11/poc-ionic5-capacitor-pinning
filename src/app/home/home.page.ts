import { Component } from "@angular/core";
import { ApiService } from "../services/api/api.service";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  constructor(private api: ApiService) {}

  handleCertificado() {
    this.api.getDataFromActualAPi().then(res => {console.log(res)});
  }

  handleNaoCertificado() {
    this.api.getDataFromFakeApi();
  }
}
