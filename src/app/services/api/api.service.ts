import { Injectable } from "@angular/core";
import { HTTP } from "@ionic-native/http/ngx";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HTTP) {}

  readonly apiEndPoint = "https://www.themealdb.com/api/json/v1/1/list.php?c=list";
  readonly fakeEndPoint = " https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
  // This will get work with SSL Pinning becuase API endpoint is same as our certificate
  getDataFromActualAPi() {
    return this.http.get(this.apiEndPoint, {}, {});
  }
  // This call will be fail as the domain is not correct according to our certificate.
  getDataFromFakeApi() {
    return this.http.get(this.fakeEndPoint, {}, {});
  }
}
